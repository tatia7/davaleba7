package com.example.davaleba7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.davaleba7.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding  : ActivityMainBinding
    private val items = mutableListOf<Items>()
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        dataSer()

        init()
    }
    private fun init()

    {
        adapter = RecyclerViewAdapter(items)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)

        binding.recyclerView.adapter = adapter
    }


    private fun dataSer(){
        Handler().postDelayed({
            items.add(Items(R.mipmap.ic_hk, "Hello Kitty", "cat with red bow"))
            items.add(Items(R.mipmap.ic_bt_foreground, "Bad Temaru", "male penguin with spiky hair"))
            items.add(Items(R.mipmap.ic_cc_foreground, "Choco Cat", "black cat with black huge eyes"))
            items.add(Items(R.mipmap.ic_k_foreground, "Keroppi", "frog with large eyes"))
            items.add(Items(R.mipmap.ic_k2_foreground, "Kuromi", "white rabbit-like creature"))
            items.add(Items(R.mipmap.ic_mm_foreground, "My Melody", "rabbit who always wears pink"))
            adapter.notifyDataSetChanged()
        },2000)
    }
}