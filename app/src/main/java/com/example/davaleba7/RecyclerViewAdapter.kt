package com.example.davaleba7

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.davaleba7.databinding.ActivityItemsBinding


class RecyclerViewAdapter(private  val items:MutableList<Items>): RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.activity_items, parent, false)
        val itemView2 = ActivityItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewHolder(itemView2)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ItemViewHolder(val binding : ActivityItemsBinding) : RecyclerView.ViewHolder(binding.root){
        private lateinit var model: Items
        fun bind(){
            model = items[adapterPosition]
            binding.image.setImageResource(model.image)
            binding.text.text = model.title
            binding.description.text = model.description
        }
    }
}